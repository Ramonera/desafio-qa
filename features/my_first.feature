Feature: Test features of whatsapp

  Scenario: Create a new Group
    Given I am on "WhatsApp"
    When I press "Options"
    And I select the group participants
    And I give the group a "name"
    Then I see the group "name" and the participants "name"


  Scenario: Start a new conversation
    Given I am on "WhatsApp"
    When I press "New Message"
    And I press "Search" to search a contact "name"
    And I press the contact "name" on the list
    Then I press the "text field" and send the "message"

  Scenario: Exit a Group
    Given i am on "WhatsApp"
    When I press "Group"
    And I press "The Group name"
    And I scroll to "Leave the group"
    Then I confirm pressing in "Leave"

  Scenario: Delete a Group
    Given I am on "WhatsApp"
    When I press "Group"
    And I press "Erase Group"
    Then I confirm pressing in "Erase"

  Scenario: Delete a chat
    Given I am on "WhatsApp"
    When I press "Chat Name"
    And I press "Options"
    And I press "More"
    Then I press "Erase Chat" and confirm the action pressing "Erase"
