Given(/^I am on "(.*?)"/) do |arg1|
  wait_for_element_exists("* marked:'#{arg1}'")
end

When(/^I press "(.*?)"$/) do |arg1|
  Interactions.new.click(arg1)
end

When(/^I select the "(.*?)" of the group participants/) do |arg1|
  Interactions.new.click(arg1)
  Interactions.new.click('btn_next')
end

When(/^I give the group "(.*?)"$/) do |arg1|
  Interactions.new.type("edit_text",arg1)
end

Then(/^I see the group "(.*?)" and the participants "(.*?)"$/) do |arg1,arg2|
  wait_for_element_exists("* id:'#{arg1}'")
  wait_for_element_exists("* id:'#{arg2}'")
end


When(/^I press "(.*?)" to search a contact "(.*?)"$/) do |arg1,arg2|
  Interactions.new.type(arg1,arg2)
end

When(/^I press the contact "(.*?)"on the list$/) do |arg1|
  Interactions.new.click(arg1)
end

Then(/^I click the "(.*?)" and send the "(.*?)"$/) do |arg1,arg2|
  Interactions.new.type(arg1,arg2)
end

When(/^I scroll to "(.*?)"$/) do |arg1|
  Interactions.new.scroll(arg1)
  Interactions.new.click(arg1)
end

Then(/^I confirm pressing in "(.*?)"$/) do |arg1|
  Interactions.new.click(arg1)
end

Then(/^I press "(.*?)" and confirm the action pressing "(.*?)"$/) do |arg1,arg2|
  Interactions.new.click(arg1)
  Interactions.new.click(arg2)
end
