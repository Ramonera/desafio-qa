require_relative 'checkout'

RSpec.describe CheckOut do
  rules = {
    'A': 50,
    'B': 30,
    'C': 20,
    'D': 15
  }

  it 'Check each unitPrice Calculation' do
    @checkout = CheckOut.new(rules)

    expect(@checkout.scan('A')).to eq(50)
    expect(@checkout.scan('B')).to eq(30)
    expect(@checkout.scan('C')).to eq(20)
    expect(@checkout.scan('D')).to eq(15)
  end

  it 'Check each specialPrice Calculation' do
    @checkout = CheckOut.new(rules)

    expect(@checkout.scan('AAA')).to eq(130)
    expect(@checkout.scan('BB')).to eq(45)
  end

  it 'Check the sum values from different bills variations with unitPrices only' do
    @checkout = CheckOut.new(rules)

    expect(@checkout.scan('AB')).to eq(80)
    expect(@checkout.scan('AC')).to eq(70)
    expect(@checkout.scan('AD')).to eq(65)
    expect(@checkout.scan('BC')).to eq(50)
    expect(@checkout.scan('BD')).to eq(45)
    expect(@checkout.scan('CD')).to eq(35)
    expect(@checkout.scan('ABC')).to eq(100)
    expect(@checkout.scan('BCD')).to eq(65)
    expect(@checkout.scan('ABD')).to eq(95)
    expect(@checkout.scan('ABCD')).to eq(115)
  end

  it 'Check the sum values from different bills variations with specialPrices only' do
    @checkout = CheckOut.new(rules)

    expect(@checkout.scan('AAABB')).to eq(175)
    expect(@checkout.scan('AAAAAABBBB')).to eq(350)
    expect(@checkout.scan('AAAAAAAAABBBBBB')).to eq(525)
    expect(@checkout.scan('AAAAAABB')).to eq(305)
    expect(@checkout.scan('AAABBBB')).to eq(220)
  end

  it 'Check the sum values from different bills variations' do
    @checkout = CheckOut.new(rules)

    expect(@checkout.scan('AAABBB')).to eq(205)
    expect(@checkout.scan('AAAABBB')).to eq(255)
    expect(@checkout.scan('AAAAABBBB')).to eq(320)
    expect(@checkout.scan('AAABBBBB')).to eq(250)
    expect(@checkout.scan('AAABBBBBBB')).to eq(295)
    expect(@checkout.scan('AAAAAAABBBBB')).to eq(430)
  end

end
