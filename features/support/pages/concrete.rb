class Interactions
  include Calabash::Android::Operations

  def click(arg1)
    touch("* id:'#{arg1}'")
  end

  def type(arg1,arg2)
    touch("* id:'#{arg1}'")
    keyboard_enter_text(arg2)
  end

  def scroll(arg1)
    until element_exists("* marked:'#{arg1}'") do
      scroll("ScrollView", :down)
    end
  end

end
