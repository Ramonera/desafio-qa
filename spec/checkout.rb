class CheckOut

  def initialize(rules)
    @rules = rules
  end

  def scan(values)
    @sum = 0
    @values = values.scan(/\w/)
    first_special_price
    second_special_price
    quantity_c = 0
    quantity_c = @values.count('C') if @values.include?("C")
    @sum += quantity_c * @rules[:'C']
    quantity_d = 0
    quantity_d = @values.count('D') if @values.include?("D")
    @sum += quantity_d * @rules[:'D']
  end

  private

  def first_special_price
    count = 0
    if @values.include?('A')
      count = @values.count('A')
      first_spec_rules(count)
    end
  end

  def first_spec_rules(count)
    if count >= 3
      quocient = count % 3
      part_of_sum = count / 3
      @sum += quocient * @rules[:'A']
      @sum += part_of_sum * 130
    else
      @sum += count * @rules[:'A']
    end
  end

  def second_special_price
    count = 0
    if @values.include?('B')
      count = @values.count('B')
      second_spec_rules(count)
    end
  end

  def second_spec_rules(count)
    if count >= 2
      quocient = count % 2
      part_of_sum = count / 2
      @sum += quocient * @rules[:'B']
      @sum += part_of_sum * 45
    else
      @sum += count * @rules[:'B']
    end
  end

end
